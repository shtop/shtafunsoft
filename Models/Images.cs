﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Reflection;
using System.IO;

namespace test.Models
{
    public class Tree
    {
        public static Bitmap NewImage(Image OriginalBitmap, int Width, int Height, bool WithPad)
        {
            Bitmap Img = new Bitmap(Width, Height);
            Graphics graphic = Graphics.FromImage(Img);
            graphic.Clear(Color.Black);
            if (WithPad)
            {
                Rectangle srcRect = new Rectangle(0, 0, Width, Height);
                Rectangle destRect = new Rectangle(Padding.Left, Padding.Top, Width - Padding.Rigth - Padding.Left, Height - Padding.Top - Padding.Bottom);
                GraphicsUnit units = GraphicsUnit.Pixel;
                graphic.DrawImage(OriginalBitmap, destRect, srcRect, units);
            }
            else
            {
                graphic.DrawImage(OriginalBitmap, 0, 0, Width, Height);
            }
            return Img;
        }

        public static Bitmap ImageFromRow(List<Image> curRow, int Width, int Height)
        {
            Bitmap Img = new Bitmap(Width, Height);
            Graphics g = Graphics.FromImage(Img);
            int tmpWidth = 0;
            foreach (Image img in curRow)
            {
                g.DrawImage(img, new Rectangle(tmpWidth, 0, img.Width, img.Height));
                tmpWidth += img.Width;
            }
            return Img;
        }

        public static Bitmap ImageFromCol(List<Image> curCol, int Width, int Height)
        {
            Bitmap Img = new Bitmap(Width, Height);
            Graphics g = Graphics.FromImage(Img);
            int tmpHeight = 0;
            foreach (Image img in curCol)
            {
                g.DrawImage(img, new Rectangle(0, tmpHeight, img.Width, img.Height));
                tmpHeight += img.Height;
            }
            return Img;
        }
    }

    public static class Padding
    {
        public static int Top { get; set; }
        public static int Bottom { get; set; }
        public static int Left { get; set; }
        public static int Rigth { get; set; }

        public static void SetPadd(int padd)
        {
            Padding.Top = padd;
            Padding.Bottom = padd;
            Padding.Left = padd;
            Padding.Rigth = padd;
        }
    }

    public class Column
    {
        public List<Row> row { get; set; }
        public Image Img { get; set; }
        public bool isRow { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }

    public class Row
    {
        public List<Column> column { get; set; }
        public Image Img { get; set; }
        public bool isColumn { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }

    public class HardTree
    {
        public List<Row> row { get; set; }
        public List<Column> column { get; set; }
        public string StoryboardPath = "/Content/images/tmp/0.jpeg";

        public void SetRow()
        {
            string rootpath = System.Web.HttpContext.Current.Server.MapPath("~");
            var dir = new DirectoryInfo(rootpath + "/Content/images/");
            this.row = new List<Row>();
            foreach (FileInfo f in dir.GetFiles())
            {
                this.row.Add(new Row() { Img = Image.FromFile(f.FullName), isColumn = false });
            }
        }


        public void SetTree()
        {
            List<Row> r1 = new List<Row>();
            List<Row> r2 = new List<Row>();
            List<Column> c1 = new List<Column>();
            List<Column> c2 = new List<Column>();
            string imgpath = "/Content/images/";
            string rootpath = System.Web.HttpContext.Current.Server.MapPath("~") + imgpath;
            var dir = new DirectoryInfo(rootpath);
            List<Image> AllImg = new List<Image>();
            foreach (FileInfo f in dir.GetFiles())
            {
                AllImg.Add(Image.FromFile(f.FullName));
            }
            r1.Add(new Row() { Img = AllImg[5], isColumn = false });
            r1.Add(new Row() { column = c1, isColumn = true });
            r1.Add(new Row() { Img = AllImg[1], isColumn = false });
            c1.Add(new Column() { row = r2, isRow = true });
            c1.Add(new Column() { Img = AllImg[2], isRow = false });
            r2.Add(new Row() { Img = AllImg[3], isColumn = false });
            r2.Add(new Row() { Img = AllImg[4], isColumn = false });
            r2.Add(new Row() { column = c2, isColumn = true });
            c2.Add(new Column() { Img = AllImg[0], isRow = false });
            c2.Add(new Column() { Img = AllImg[6], isRow = false });
            this.row = r1;
        }

        public void Storyboard(List<Row> row, int FinalWidth)
        {
            List<int> Heights = new List<int>();
            Bitmap tmpImg;
            int FinalHeight = 0;
            double WidthToFinal = 0;
            Bitmap FinalStoryboard;
            tmpImg = ImageFromRow(row); //формирование временной раскадровки для вычисления пропорций
            DoMinRow(row);  // маштабирование всех элементов строки к минимальнуму из них
            FinalHeight = (FinalWidth * tmpImg.Height) / tmpImg.Width;
            WidthToFinal = (double)FinalWidth / (double)tmpImg.Width; // отношение размеров временной картинки к окончательной
            SizeRow(row, WidthToFinal); // изменение размеров всех элементов к окончательной картинки
            AddPaddingsRow(row); // добавление отступов
            tmpImg = ImageFromRow(row);                                                                 //
            FinalStoryboard = Tree.NewImage(tmpImg, FinalWidth, FinalHeight, false);                    // формирование окончательной картинки
            FinalStoryboard.Save(System.Web.HttpContext.Current.Server.MapPath("~") + StoryboardPath);  //
        }


        private void AddPaddingsRow(List<Row> row)
        {
            foreach (var item in row)
            {
                if (item.isColumn)
                {
                    AddPaddingsCol(item.column);
                }
                else
                {
                    item.Img = Tree.NewImage(item.Img, item.Img.Width, item.Img.Height, true);
                }
            }
        }

        private void AddPaddingsCol(List<Column> col)
        {
            foreach (var item in col)
            {
                if (item.isRow)
                {
                    AddPaddingsRow(item.row);
                }
                else
                {
                    item.Img = Tree.NewImage(item.Img, item.Img.Width, item.Img.Height, true);
                }
            }
        }

        private void SizeRow(List<Row> row, double WidthToFinal)
        {
            int Width = 0, Height = 0;
            foreach (var item in row)
            {
                if (item.isColumn)
                {
                    SizeCol(item.column, WidthToFinal);
                }
                else
                {
                    Width = (int)(item.Width * WidthToFinal);
                    Height = (int)(item.Height * WidthToFinal);
                    item.Img = Tree.NewImage(item.Img, Width, Height, false);
                }
            }
        }

        private void SizeCol(List<Column> col, double WidthToFinal)
        {
            int Width = 0, Height = 0;
            foreach (var item in col)
            {
                if (item.isRow)
                {
                    SizeRow(item.row, WidthToFinal);
                }
                else
                {
                    Width = (int)(item.Width * WidthToFinal);
                    Height = (int)(item.Height * WidthToFinal);
                    item.Img = Tree.NewImage(item.Img, Width, Height, false);
                }
            }
        }

        private static Bitmap ImageFromCol(List<Column> col)
        {
            int MinWidth = 0;
            int tmpHeight = 0;
            int Height = 0;
            List<int> Widths = new List<int>();
            Bitmap tmpImg;
            List<Image> tmpCol = new List<Image>();
            List<Image> FinalCol = new List<Image>();
            double WidthToMax = 0;
            foreach (var item in col)
            {
                if (item.isRow)
                {
                    tmpImg = ImageFromRow(item.row);
                    Widths.Add(tmpImg.Width);
                    tmpCol.Add(tmpImg);
                }
                else
                {
                    Widths.Add(item.Img.Width);
                    tmpCol.Add(item.Img);
                }
            }

            MinWidth = Widths.Min();

            foreach (var item in col)
            {
                if (item.isRow == false)
                {
                    WidthToMax = (double)item.Img.Width / (double)MinWidth;
                    tmpHeight = (int)(item.Img.Height / WidthToMax);
                    item.Height = tmpHeight;
                    item.Width = MinWidth;
                }
            }

            foreach (Image img in tmpCol)
            {
                WidthToMax = (double)img.Width / (double)MinWidth;
                tmpHeight = (int)(img.Height / WidthToMax);
                Height += tmpHeight;
                tmpImg = Tree.NewImage(img, MinWidth, tmpHeight, false);
                FinalCol.Add(tmpImg);
            }

            tmpImg = Tree.ImageFromCol(FinalCol, MinWidth, Height);
            return tmpImg;
        }

        private static Bitmap ImageFromRow(List<Row> row)
        {
            int MinHeight = 0;
            int tmpWidth = 0;
            int Width = 0;
            List<int> Heights = new List<int>();
            Bitmap tmpImg;
            List<Image> tmpRow = new List<Image>();
            List<Image> FinalRow = new List<Image>();
            double HeightToMax = 0;
            foreach (var item in row)
            {
                if (item.isColumn)
                {
                    tmpImg = ImageFromCol(item.column);
                    Heights.Add(tmpImg.Height);
                    tmpRow.Add(tmpImg);
                }
                else
                {
                    Heights.Add(item.Img.Height);
                    tmpRow.Add(item.Img);
                }
            }

            MinHeight = Heights.Min();

            foreach (var item in row)
            {
                if (item.isColumn == false)
                {
                    HeightToMax = (double)item.Img.Height / (double)MinHeight;
                    tmpWidth = (int)(item.Img.Width / HeightToMax);
                    item.Height = MinHeight;
                    item.Width = tmpWidth;
                }
            }

            foreach (Image img in tmpRow)
            {
                HeightToMax = (double)img.Height / (double)MinHeight;
                tmpWidth = (int)(img.Width / HeightToMax);
                Width += tmpWidth;
                tmpImg = Tree.NewImage(img, tmpWidth, MinHeight, false);
                FinalRow.Add(tmpImg);
            }

            tmpImg = Tree.ImageFromRow(FinalRow, Width, MinHeight);
            return tmpImg;
        }
        private static int ColumnHeight(List<Column> col)
        {
            int Height = 0;
            foreach (var item in col)
            {
                if (item.isRow)
                {
                    Height += RowHeight(item.row);
                }
                else
                {
                    Height += item.Height;
                }
            }

            return Height;
        }

        private static int RowHeight(List<Row> row)
        {
            int Height = 0;
            foreach (var item in row)
            {
                if (item.isColumn)
                {
                    Height = ColumnHeight(item.column);
                }
                else
                {
                    Height = item.Height;
                }
            }
            return Height;
        }


        private static void DoMinRow(List<Row> row)
        {
            List<int> Heights = new List<int>();
            int MinHeight = 0, tmpWidth = 0;
            double HeightToMax = 0;
            foreach (var item in row)
            {
                if (item.isColumn)
                {
                    Heights.Add(ColumnHeight(item.column));
                }
                else
                {
                    Heights.Add(item.Height);
                }
            }

            MinHeight = Heights.Min();

            foreach (var item in row)
            {
                if (item.isColumn)
                {
                    HeightToMax = (double)ColumnHeight(item.column) / (double)MinHeight;
                    SetMinColHeight(item.column, HeightToMax);
                }
                else
                {
                    HeightToMax = (double)item.Height / (double)MinHeight;
                    tmpWidth = (int)(item.Width / HeightToMax);
                    item.Height = MinHeight;
                    item.Width = tmpWidth;
                }
            }
        }

        private static int RowWidth(List<Row> row)
        {
            int Width = 0;
            foreach (var item in row)
            {
                if (item.isColumn)
                {
                    Width += ColumnWidth(item.column);
                }
                else
                {
                    Width += item.Width;
                }
            }

            return Width;
        }

        private static void SetMinRowWidth(List<Row> row, double WidthToMax)
        {
            foreach (var item in row)
            {
                if (item.isColumn)
                {
                    SetMinColWidth(item.column, WidthToMax);
                }
                else
                {
                    item.Height = (int)(item.Height / WidthToMax);
                    item.Width = (int)(item.Width / WidthToMax);
                }
            }
        }

        private static void SetMinColWidth(List<Column> col, double WidthToMax)
        {
            int MaxWidth = 0, Width = 0;
            List<int> Widths = new List<int>();
            foreach (var item in col)
            {
                if (item.isRow)
                {
                    Widths.Add(RowWidth(item.row));
                }
                else
                {
                    Widths.Add(item.Width);
                }
            }

            MaxWidth = Widths.Max();

            foreach (var item in col)
            {
                if (item.isRow)
                {
                    if (RowWidth(item.row) == MaxWidth)
                    {
                        Width = (int)(RowWidth(item.row) / WidthToMax);
                    }
                }
                else
                {
                    if (item.Width == MaxWidth)
                    {
                        Width = (int)(item.Width / WidthToMax);
                    }
                }
            }

            foreach (var item in col)
            {
                if (item.isRow)
                {
                    WidthToMax = (double)Width / (double)RowWidth(item.row);
                    SetMinRowWidth(item.row, WidthToMax);
                }
                else
                {
                    WidthToMax = (double)Width / (double)item.Width;
                    item.Height = (int)(item.Height * WidthToMax);
                    item.Width = (int)(item.Width * WidthToMax);
                }
            }
        }

        private static void SetMinRowHeight(List<Row> row, double HeightToMax)
        {
            int MaxHeight = 0, Height = 0;
            List<int> Heights = new List<int>();
            foreach (var item in row)
            {
                if (item.isColumn)
                {
                    Heights.Add(ColumnHeight(item.column));
                }
                else
                {
                    Heights.Add(item.Height);
                }
            }

            MaxHeight = Heights.Max();

            foreach (var item in row)
            {
                if (item.isColumn)
                {
                    if (ColumnHeight(item.column) == MaxHeight)
                    {
                        Height = (int)(ColumnHeight(item.column) / HeightToMax);
                    }
                }
                else
                {
                    if (item.Height == MaxHeight)
                    {
                        Height = (int)(item.Height / HeightToMax);
                    }
                }
            }

            foreach (var item in row)
            {
                if (item.isColumn)
                {
                    HeightToMax = (double)ColumnHeight(item.column) / (double)Height;
                    SetMinColHeight(item.column, HeightToMax);
                }
                else
                {
                    HeightToMax = (double)Height / (double)item.Height;
                    item.Height = (int)(item.Height * HeightToMax);
                    item.Width = (int)(item.Width * HeightToMax);
                }
            }
        }

        private static void SetMinColHeight(List<Column> col, double HeightToMax)
        {
            foreach (var item in col)
            {
                if (item.isRow)
                {
                    SetMinRowHeight(item.row, HeightToMax);
                }
                else
                {
                    item.Height = (int)(item.Height / HeightToMax);
                    item.Width = (int)(item.Width / HeightToMax);
                }
            }
        }


        private static int ColumnWidth(List<Column> col)
        {
            int Width = 0;
            foreach (var item in col)
            {
                if (item.isRow)
                {
                    Width = RowWidth(item.row);
                }
                else
                {
                    Width = item.Width;
                }
            }

            return Width;
        }
    }
}
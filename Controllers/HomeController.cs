﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using test.Models;

namespace test.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EasyTree()
        {
            HardTree HT = new HardTree();
            Padding.SetPadd(0);
            HT.SetRow();
            HT.Storyboard(HT.row, 2000);
            return View(HT);
        }

        public ActionResult HardTree()
        {
            HardTree HT = new HardTree();
            Padding.SetPadd(0);
            HT.SetTree();
            HT.Storyboard(HT.row, 1600);
            return View(HT);
        }

        public ActionResult HardTreePad()
        {
            HardTree HT = new HardTree();
            Padding.Top = 20;
            Padding.Bottom = 10;
            Padding.Left = 5;
            Padding.Rigth = 15;
            HT.SetTree();
            HT.Storyboard(HT.row, 1600);
            return View(HT);
        }

    }
}
